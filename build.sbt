name := "koresframework-scala"
organization := "com.koresframework"
version := "0.1.4"
versionScheme := Some("semver-spec")

enablePlugins(GitlabPlugin)

scalaVersion := "3.0.2"

resolvers += "GitLab-JwIUtils" at "https://gitlab.com/api/v4/projects/28895078/packages/maven"
resolvers += "GitLab-Kores" at "https://gitlab.com/api/v4/projects/28894889/packages/maven"

// Runtime
libraryDependencies += ("com.github.jonathanxd" % "jwiutils" % "4.17.8")
libraryDependencies += ("com.koresframework" % "kores" % "4.2.8.base")

// Test
libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.9" % Test

organizationName := "KoresFramework"
startYear := Some(2014)
licenses += ("MIT", new URL("https://opensource.org/licenses/MIT"))
headerLicense := Some(HeaderLicense.MIT("2021", "KoresFramework"))