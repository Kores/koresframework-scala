package com.koresframework.sc.ala.test

import com.koresframework.sc.ala.util.typeOf
import com.koresframework.sc.ala.util.genericTypeOf
import com.koresframework.sc.ala.util.koresType
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest._
import flatspec._
import matchers._

class TypeUtilTest extends AnyFunSuite with should.Matchers {
  test("typeOf[String]") {
      val stringType = typeOf[String].koresType
      assert(stringType.getIdentification == "Ljava/lang/String;")
  }
  test("genericTypeOf[List[String]]") {
      val stringType = genericTypeOf[List[String]]
      assert(stringType.getIdentification == "Lscala/collection/immutable/List<Ljava/lang/String;>;")
  }
}
